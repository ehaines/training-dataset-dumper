#include "JetWriters/JetTrackWriter.h"
#include "JetWriters/JetConstituentWriter.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODTracking/TrackParticle.h"
#include "xAODJet/Jet.h"

///////////////////////////////////
// Class definition starts here
///////////////////////////////////
JetTrackWriter::JetTrackWriter(
  H5::Group& output_file,
  const JetConstituentWriterConfig& cfg):
  m_writer(nullptr)
{
  if (cfg.name.size() == 0) {
    throw std::logic_error("output name not specified");
  }
  if (cfg.size != 0) {
    m_writer.reset(
      new JetConstituentWriter<xAOD::TrackParticle>(output_file, cfg));
  }
}

JetTrackWriter::~JetTrackWriter() {
  flush();
}

JetTrackWriter::JetTrackWriter(JetTrackWriter&&) = default;

void JetTrackWriter::write(const xAOD::Jet& jet,
                           const JetTrackWriter::Tracks& tracks) {
  if (m_writer) m_writer->write(jet, tracks);
}

void JetTrackWriter::flush() {
  if (m_writer) m_writer->flush();
}
