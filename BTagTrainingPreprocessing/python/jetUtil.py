from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

###############################################################
# Function to apply jet systematics
###############################################################

def applyJetSys(sys_list, jet_collection, jet_sigma):
    ca = ComponentAccumulator()
   
    jetUncertTool = CompFactory.JetUncertaintiesTool()
    if jet_collection=="AntiKt4EMPFlowJetsSys":
        jetUncertTool.ConfigFile = "rel21/Summer2019/R4_SR_Scenario1_SimpleJER.config"
    if jet_collection=="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets":
        jetUncertTool.ConfigFile = "rel21/Fall2022/R10_CategoryReduction.config"
    jetUncertTool.MCType = "MC16"
    jetUncertTool.IsData = False
    jetUncertTool.JetDefinition = jet_collection.replace("Jets", "").rstrip("Sys")
    # Check the configs here: /GroupData/JetUncertainties/CalibArea-08/rel21/Summer2019/. The names of the systematics can be found there.
    # the name of the jet systematics should be of this format: JET_Flavor_Response.The c++ implementation is rather minimal so that the user needs to ensure the correct systematic name is used. 
    jetCalibTool = CompFactory.JetCalibrationTool()
    jetCalibTool.JetCollection =  jet_collection.replace("Jets", "").rstrip("Sys")
    if jet_collection=="AntiKt4EMPFlowJetsSys":
        jetCalibTool.ConfigFile = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
    if jet_collection=="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets":
        jetCalibTool.ConfigFile = "JES_MC16recommendation_R10_UFO_CSSK_SoftDrop_JMS_Insitu_30Sep2022.config"
    jetCalibTool.IsData = False
    jetCalibTool.CalibSequence = "JetArea_Residual_EtaJES_GSC_Smear"

    ca.addPublicTool(jetUncertTool)
    ca.addPublicTool(jetCalibTool)
    
    jetSysAlg = CompFactory.JetSystematicsAlg("JetSysAlg")
    jetSysAlg.jet_collection = jet_collection.replace("Sys", "")
    jetSysAlg.systematic_variations  = sys_list
    jetSysAlg.sigma  = jet_sigma
    
    jetSysAlg.jet_uncert_tool = jetUncertTool
    jetSysAlg.jet_calib_tool = jetCalibTool
 
    ca.addEventAlgo(jetSysAlg)
    return ca

