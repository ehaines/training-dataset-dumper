#!/usr/bin/env python

"""Kfold dumpster script

This script decorates jets with hash so that they can be used for
kfold evaluation. The kfold GN2v01 is also run.

"""

import sys
import json
from BTagTrainingPreprocessing import dumper
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator


def get_folder():
    ca = ComponentAccumulator()
    common = dict(
        eventID='EventInfo.mcEventNumber',
        salt=42,
        jetCollection='AntiKt4EMPFlowJets',
        associations=['constituentLinks', 'GhostTrack'],
        jetVariableSaltSeeds={
            'constituentLinks': 0,
            'GhostTrack': 1,
        },
    )
    ca.addEventAlgo(
        CompFactory.FoldDecoratorAlg(
            'withHits',
            **common,
            constituentChars=json.dumps({
                'GhostTrack': [
                    'numberOfPixelHits',
                    'numberOfSCTHits',
                    # 'numberOfTRTHits'
                ]
            }),
            constituentSaltSeeds={
                'numberOfPixelHits': 2,
                'numberOfSCTHits': 3,
                # 'numberOfTRTHits': 4,
            },
            jetFoldHash='jetFoldHash',
        )
    )

    ca.addEventAlgo(
        CompFactory.FoldDecoratorAlg(
            'noHits',
            **common,
            jetFoldHash='jetFoldHash_noHits',
        )
    )

    return ca

def run():

    parser = dumper.base_parser(__doc__)
    args = parser.parse_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    ca = dumper.getMainConfig(cfgFlags, args)
    ca.merge(get_folder())

    nn_base = 'BTagging/20231205/GN2v01/antikt4empflow'
    pf_nns = [f'{nn_base}/network_fold{n}.onnx' for n in range(4)]

    jetCollection = 'AntiKt4EMPFlow'
    BTagCollection = cfgFlags.BTagging.OutputFiles.Prefix + jetCollection

    sub = ComponentAccumulator()
    sub.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.BTagDecoratorAlg(
            name='myGN2',
            container=BTagCollection,
            constituentContainer='InDetTrackParticles',
            decorator=CompFactory.FlavorTagDiscriminants.MultifoldGNNTool(
                name='myGN2_tool',
                foldHashName='jetFoldHash',
                nnFiles=pf_nns,
                flipTagConfig="STANDARD",
            )
        )
    )
    ca.merge(sub)


    ca.merge(dumper.getDumperConfig(args))
    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
