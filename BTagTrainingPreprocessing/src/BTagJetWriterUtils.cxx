#include "BTagJetWriterUtils.hh"
#include "BTagJetWriterConfig.hh"

#include "HDF5Utils/Writer.h"

#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODBTagging/BTaggingUtilities.h"

#include <regex>

template<typename I, typename O = I>
void add_btag_fillers(JetConsumers&,
                      const std::vector<std::string>&,
                      const BTagVariableMaps&,
                      O default_value,
                      const std::string& btag_link,
                      H5Utils::Compression compression);

template<typename I, typename O = I>
void add_jet_fillers(JetConsumers&,
                     const std::vector<std::string>&,
                     O default_value,
                     H5Utils::Compression compression);

namespace {
  void extract_and_add_float(JetConsumers& fillers,
                             std::vector<std::string>& var_list,
                             std::function<float(const xAOD::Jet&)> fillfunc,
                             const std::string& name,
                             H5Utils::Compression compression) {
    auto itr = std::find(var_list.begin(), var_list.end(), name);
    if (itr == var_list.end()) return;

    std::function filler = [fillfunc](const JetOutputs& jo) -> float {
      if (!jo.jet) return NAN;
      return fillfunc(*jo.jet);
    };
    fillers.add(name, filler, NAN, compression);
    var_list.erase(itr);
  }

  void add_jet_floats(JetConsumers& fillers,
                      std::vector<std::string> unused_jet_floats,
                      H5Utils::Compression c)
  {
    extract_and_add_float(
      fillers, unused_jet_floats,
      [](const auto& j){ return j.pt(); }, "pt", c);
    extract_and_add_float(
      fillers, unused_jet_floats,
      [](const auto& j){ return j.eta(); }, "eta", c);
    extract_and_add_float(
      fillers, unused_jet_floats,
      [](const auto& j){ return std::abs(j.eta()); }, "abs_eta", c);
    extract_and_add_float(
      fillers, unused_jet_floats,
      [](const auto& j){ return j.e(); }, "energy", c);
    extract_and_add_float(
      fillers, unused_jet_floats,
      [](const auto& j){ return j.m(); }, "mass", c);
    if (unused_jet_floats.size() > 0) {
      add_jet_fillers<float>(fillers, unused_jet_floats, NAN, c);
    }
  }

}

JetOutputs::JetOutputs():
  jet(nullptr),
  parent(nullptr),
  event_info(nullptr)
{}

void add_jet_variables(JetConsumers& fillers,
                       const BTagJetWriterBaseConfig& cfg,
                       H5Utils::Compression h) {
  using uint = unsigned int;
  using uchar = unsigned char;

  H5Utils::Compression s = H5Utils::Compression::STANDARD;

  const BTagVariableMaps& m = cfg.variable_maps;
  const std::string& l = cfg.btagging_link;
  // in this case we convert doubles to floats to save space,
  // note that you can change the second parameter to a double
  // to get full precision.
  const VariablesByType& btag = cfg.btagging;
  add_btag_fillers<double, float>(fillers, btag.doubles, m, NAN, l, s);
  add_btag_fillers<float>(fillers, btag.floats, m, NAN, l, s);
  add_btag_fillers<float>(fillers, btag.halves, m, NAN, l, h);
  add_btag_fillers<uint>(fillers, btag.uints, m, 0, l, s);
  add_btag_fillers<int>(fillers, btag.ints, m, -1, l, s);
  add_btag_fillers<uchar, uchar>(fillers, btag.uchars, m, 0, l, s);
  add_btag_fillers<char, char>(fillers, btag.chars, m, -1, l, s);
  add_btag_fillers<int, float>(
    fillers, btag.ints_as_halves, m, NAN, l, h);

  add_jet_floats(fillers, cfg.jet.floats, s);
  add_jet_floats(fillers, cfg.jet.halves, h);
  add_jet_fillers<double>(fillers, cfg.jet.doubles, 0, s);
  add_jet_fillers<char>(fillers, cfg.jet.chars, 0, s);
  add_jet_fillers<uint, uint>(fillers, cfg.jet.uints, 0, s);
  add_jet_fillers<uchar, uchar>(fillers, cfg.jet.uchars, 0, s);
  add_jet_int_variables(fillers, cfg.jet.ints);
  add_jet_fillers<int, float>(
    fillers, cfg.jet.ints_as_halves, NAN, h);

}


void add_jet_int_variables(JetConsumers& vars,
                           const std::vector<std::string>& names) {
  for (const auto& label_name: names) {
    // accomidate some custom cases for jet ints
    if (label_name == "numConstituents") {
      auto f = [](const JetOutputs& jo) -> int {
                 if (!jo.jet) return -1;
                 return jo.jet->numConstituents();
               };
      vars.add<int>(label_name, f);
    } else {
      // if nothing else works, we end up here
      SG::AuxElement::ConstAccessor<int> lab(label_name);
      std::function<int(const JetOutputs&)> filler
        = [lab](const JetOutputs& jo) {
            if (!jo.jet) return -1;
            return lab(*jo.jet);
          };
      vars.add(label_name, filler);
    }
  }
}
void add_parent_jet_int_variables(
  JetConsumers& vars,
  const std::vector<std::string>& names) {
  for (const auto& label_name: names) {
    SG::AuxElement::ConstAccessor<int> lab(label_name);
    std::function<int(const JetOutputs&)> filler = [
      lab](const JetOutputs& jo) {
      if (!jo.parent) return -1;
      return lab(*jo.parent);
    };
    vars.add(label_name, filler);
  }
}

void add_parent_fillers(JetConsumers& vars) {
  FloatFiller deta = [](const JetOutputs& jo) -> float {
    if (!jo.jet || !jo.parent) return NAN;
    return jo.jet->eta() - jo.parent->eta();
  };
  vars.add("deta", deta);
  FloatFiller dphi = [](const JetOutputs& jo) -> float {
    if (!jo.jet || !jo.parent) return NAN;
    return jo.jet->p4().DeltaPhi(jo.parent->p4());
  };
  vars.add("dphi", dphi);
  FloatFiller dr = [](const JetOutputs& jo) -> float {
    if (!jo.jet || !jo.parent) return NAN;
    return jo.jet->p4().DeltaR(jo.parent->p4());
  };
  vars.add("dr", dr);
}

void add_event_info(
  JetConsumers& vars,
  const std::vector<std::string>& keys,
  H5Utils::Compression compression = H5Utils::Compression::STANDARD) {
  std::function<float(const JetOutputs&)> evt_weight = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->mcEventWeight();
  };
  std::function<long long(const JetOutputs&)> evt_number = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->eventNumber();
  };

  // store flag based on run number to indicate run2 / run3
  std::function<char(const JetOutputs&)> evt_is_run3 = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return (jo.event_info->runNumber() > 410000);
  };

  std::function<float(const JetOutputs&)> evt_mu = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->averageInteractionsPerCrossing();
  };

  std::function<float(const JetOutputs&)> evt_act_mu = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->actualInteractionsPerCrossing();
  };
  std::regex int_type("^n[A-Z].*");

  for (const auto& key: keys) {
    if (key == "mcEventWeight") {
      vars.add(key, evt_weight, NAN, compression);
    } else if (key == "eventNumber") {
      vars.add(key, evt_number);
    } else if (key == "isRun3") {
      vars.add(key, evt_is_run3);
    } else if (key == "averageInteractionsPerCrossing") {
      vars.add(key, evt_mu, NAN, compression);
    } else if (key == "actualInteractionsPerCrossing") {
      vars.add(key, evt_act_mu, NAN, compression);
    } else if (std::regex_match(key, int_type)) {
      using IA = SG::AuxElement::ConstAccessor<int>;
      std::function getter([g=IA(key)] (const JetOutputs& jo) {
        return g(*jo.event_info);
      });
      vars.add(key, getter);
    } else {
      // let's just pretend everything else is a float
      using FA = SG::AuxElement::ConstAccessor<float>;
      std::function getter([g=FA(key)] (const JetOutputs& jo) {
        return g(*jo.event_info);
      });
      vars.add(key, getter, NAN, compression);
    }
  }
}



JetOutputWriter::JetOutputWriter(H5::Group& file,
                                 const std::string& name,
                                 const JetConsumers& cons):
  m_writer(file, name, cons)
{
}
void JetOutputWriter::fill(const std::vector<JetOutputs>& jos) {
  for (const auto& jo: jos) {
    m_writer.fill(jo);
  }
}
void JetOutputWriter::flush() {
  m_writer.flush();
}


JetOutputWriter1D::JetOutputWriter1D(H5::Group& file,
                                     const std::string& name,
                                     const JetConsumers& cons,
                                     size_t n_jets):
  m_writer(file, name, cons, {n_jets})
{
}
void JetOutputWriter1D::fill(const std::vector<JetOutputs>& jos) {
  m_writer.fill(jos);
}
void JetOutputWriter1D::flush() {
  m_writer.flush();
}


// template implementation

template<typename I, typename O>
void add_btag_fillers(
  JetConsumers& vars,
  const std::vector<std::string>& names,
  const BTagVariableMaps& maps,
  O default_value,
  const std::string& btagging_link,
  H5Utils::Compression compression) {
  const auto& def_check_names = maps.replace_with_defaults_checks;
  SG::AuxElement::ConstAccessor<ElementLink<xAOD::BTaggingContainer>> blink(btagging_link.empty() ? "dummy_btag_link": btagging_link);
  for (const auto& btag_var: names) {
    SG::AuxElement::ConstAccessor<I> getter(btag_var);
    std::string out_name = btag_var;
    if (maps.rename.count(btag_var)) out_name = maps.rename.at(btag_var);
    if (def_check_names.count(btag_var)) {
      SG::AuxElement::ConstAccessor<char> def_check(
        def_check_names.at(btag_var));
      std::function<O(const JetOutputs&)> filler = [
        getter, default_value, def_check, blink](const JetOutputs& jo) -> O {
        if (!jo.jet) return default_value;
        const xAOD::BTagging* btagging = *blink(*jo.jet);
        if (!btagging) {
          throw std::logic_error("missing b-tagging container");
        }
        if (def_check(*btagging)) return default_value;
        return getter(*btagging);
      };
      vars.add(out_name, filler, default_value, compression);
    } else {
      std::function<O(const JetOutputs&)> filler = [
        getter, default_value, blink](const JetOutputs& jo) -> O {
        if (!jo.jet) return default_value;
        const xAOD::BTagging* btagging = *blink(*jo.jet);
        if (!btagging) {
          throw std::logic_error("missing b-tagging container");
        }
        return getter(*btagging);
      };
      vars.add(out_name, filler, default_value, compression);
    }
  }
}
template<typename I, typename O>
void add_jet_fillers(JetConsumers& vars,
                     const std::vector<std::string>& names,
                     O default_value,
                     H5Utils::Compression compression) {
  for (const auto& btag_var: names) {
    SG::AuxElement::ConstAccessor<I> getter(btag_var);
    std::function<O(const JetOutputs&)> filler = [
      getter, default_value](const JetOutputs& jo) -> O {
      if (!jo.jet) return default_value;
      return getter(*jo.jet);
    };
    vars.add(btag_var, filler, default_value, compression);
  }
}
