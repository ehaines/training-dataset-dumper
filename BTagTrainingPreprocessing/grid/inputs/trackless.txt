# see https://ftag-docs.docs.cern.ch/activities/trackless/

# https://its.cern.ch/jira/browse/ATLFTAGDPD-345

mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3681_r14736_r14672_p5700
mc20_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3778_r13258_r13146_p5700
