# Advanced Usage

The basic setup, described in the previous section, is capable of calculating simple variables from jets and b-tagging, and of running the neural network based flavor taggers. If you are starting from a DAOD that is likely all you need, **stop reading this and go to the basic section!** If you want to do something more complicated, read on.

The "advanced setup" supports a few additional workflows, for example:

- Augmenting release 21 (D)AODs to make them release 22 compatible,
- Extrapolating tracks to primary vertex to (re)calculate impact parameters
- Creating transient jet collections from trigger decisions, for
  trigger studies
- Building secondary vertices (i.e. "retagging") and running the full
  flavor tagging chain from untagged jets.
- Re-running track to jet association.

These workflows require you to use the Gaudi scheduler and event loop, part of the larger framework that ATLAS uses for trigger, reconstruction, and derivations.

For these cases we provide an Athena algorithm, `SingleBTagAlg`,
(technically a duel-use `EL::AnaAlgorithm`) which wraps the same
functionality as the stand alone executable.


### Installation

Further complicating matters, you can choose to build against either:

- `AthAnalysis`: the lightweight "analysis" Gaudi-based framework, or
- `Athena`: the full reconstruction framework

The lighter `AthAnalysis` framework will not support lower level reconstruction. For an exact list of which packages are included in every base project, see the `package_filters.txt` files under [the `Projects` directory in Athana][prj]. On the other hand, it's possible to run `AthAnalysis` locally via a docker image.

[prj]: https://gitlab.cern.ch/atlas/athena/-/tree/main/Projects

The training dataset dumper can be used within either release by using `setup/[project].sh` in place of
`setup/analysisbase.sh`. Make sure you rebuild your code _completely_ (delete the `build` directory) if you've changed releases!

Using the `Athena` project as an example, a local installation can be achieved with:

```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
source training-dataset-dumper/setup/athena.sh
mkdir build
cd build
cmake ../training-dataset-dumper
make
cd ..
```

??? warning "Run the installation in a clean shell."

    It is again advised to run in a clean shell with no PYTHONPATH or environment. 

As in the basic usage installation, the following file needs to be sourced in order to add the executables to the system path

```bash
source build/x*/setup.sh
```

??? warning "I get `dbm.error` when running"

  ```bash
  dbm.error: db type is dbm.gnu, but the module is not available
  ```

  This error can result from settup up within a conda environemnt.
  To fix it, run the following commands before setting up anything:

  ```bash
  conda deactivate
  unset PYTHONPATH
  ```
  